var StateIntro = {
    create: function () {
        var w = game.world.width;
        var h = game.world.height;

        this.bkg = game.add.tileSprite(0, 0, w, h, 'imgBkg');
        var margin_top = 30;
        var logo = game.add.image(0, 0, 'imgLogo');
        logo.anchor.x = .5;
        logo.x = game.world.centerX;
        logo.y = margin_top;

        var out_frame =0;
        var over_frame = 1;
        var down_frame = 2;

        var btn_start = game.add.button(0,0, 'btnStart', this.goToMain, this, over_frame, out_frame, down_frame);
        btn_start.anchor.x = .5;
        btn_start.x = game.world.centerX;
        btn_start.y = game.world.centerY;
    },
    update : function () {
        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.goToMain();
        }
        this.bkg.tilePosition.y+=1;
    },
    goToMain : function () {
        game.state.start('StateMain');
    }
};
