var StateLoad = {
    preload : function () {
        var prog_void = game.add.image(0, 0, 'imgProgVoid');
        prog_void.x = game.world.centerX - prog_void.width / 2;
        prog_void.y = game.world.centerY;

        var prog_full = game.add.image(0,0, 'imgProgFull');
        prog_full.anchor.x = prog_void.x;
        prog_full.anchor.y = prog_void.y;

        game.load.setPreloadSprite(prog_full);

        game.load.image('imgPaddle', 'img/paddle.png');
        game.load.image('imgBrickGreen', 'img/brick_green.png');
        game.load.image('imgBrickPurple', 'img/brick_purple.png');
        game.load.image('imgBrickRed', 'img/brick_red.png');
        game.load.image('imgBrickYellow', 'img/brick_yellow.png');
        game.load.image('imgBall', 'img/ball.png');

        game.load.audio('sfxHitBrick', 'snd/fx_hit_brick.wav');
        game.load.audio('sfxHitPaddle', 'snd/fx_hit_paddle.wav');
        game.load.audio('bgmMusic', 'snd/bgm_electric_air.ogg');

        // these were originally in the create method
        game.load.image('imgBkg', 'img/bg_blue.png');
        game.load.image('imgBlack', 'img/bg_black.png');

        game.load.audio('sfxLoseLife', 'snd/fx_lose_life.ogg');

        game.load.image('imgBkg', 'img/bg_blue.png');
        game.load.spritesheet('btnBack', 'img/btn_back.png', 190, 49);
        game.load.audio('sfxWin', 'snd/fx_lose.ogg');
        game.load.audio('sfxWin', 'snd/fx_lose.ogg');
        game.load.audio('sfxFirework', 'snd/fx_firework.ogg');
        game.load.image('imgStar', 'img/star.png')

        game.load.image('imgBkg', 'img/bg_blue.png');
        game.load.image('imgLogo', 'img/logo_game.png');
        game.load.spritesheet('btnStart', 'img/btn_start.png', 190, 49);
    },
    create : function () {
        game.state.start('StateIntro');
    }
};
