var StateOver = {
    create : function () {
        var points = game.points;
        var lives = game.lives;
        var w = game.world.width;
        var h = game.world.height;
        this.bkg = game.add.tileSprite(0, 0, w, h, 'imgBkg');
        var out_frame = 0;
        var over_frame = 1;
        var down_frame = 2;
        var margin =30;
        var btn_back = game.add.button(0, 0, 'btnBack', this.goToIntro, this, over_frame, out_frame, down_frame);
        btn_back.anchor.set(.5,1);
        btn_back.x = game.world.centerX;
        btn_back.y = game.world.height - margin;

        this.sfx_firework = game.add.audio('sfxFirework');
        var max_particles = 100;
        this.fireworks = game.add.emitter(0, 0, max_particles);
        this.fireworks.makeParticles('imgStar');
        this.fireworks.gravity.y = 500;

        this.top_time = 1;
        this.timer = this.top_time;

        var txt_over_config = {
            font: '40px Overlock',
            fill : '#ffffff',
            align:'center'
        };
        var txt_over = game.add.text(0,0,g_text_game_over, txt_over_config);
        txt_over.anchor.x = .5;
        txt_over.x = game.world.centerX;
        txt_over.y = margin * 2;

        var sfx_win_lose;
        if (lives > 0) {
            txt_over.fill = '#e0d700';
            txt_over.text = g_text_congrats;
            sfx_win_lose = game.add.audio('sfxWin');
        } else {
            sfx_win_lose = game.add.audio('sfxLose');
        }
        sfx_win_lose.play();

        var txt_points_config = {
            font : '28px Overlock',
            fill : '#ffffff',
            align : 'center'
        };
        var txt_points = game.add.text(0,0, points + g_txt_points, txt_points_config);
        txt_points.anchor.x = .5;
        txt_points.x = game.world.centerX;
        txt_points.y = game.world.centerY - margin;

    },
    update : function () {
        if (game.lives ==0) {
            return;
        }
        this.timer -= game.time.physicsElapsed;
        if (this.timer < 0) {
            this.timer = this.top_time;
        }
        var rand_x = Math.random() * game.world.width;
        var rand_y = Math.random() * game.world.height;
        this.fireworks.x = rand_x;
        this.fireworks.y = rand_y;
        var duration = 800;
        var num_stars = 10;

        this.fireworks.start(true, duration, null, num_stars);
        this.sfx_firework.play();

        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.goToIntro();
        }
    },
    goToIntro: function () {
        game.state.start('StateIntro');
    }
};
