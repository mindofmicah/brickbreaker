var StateMain = {
    create : function () {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.checkCollision.down = false;

        this.touchOldX = undefined;
        this.touchNewX = undefined;
        this.touchActive = false;
        this.touchMove = 0;

        this.lives = 3;
        this.points = 0;

        var w = game.world.width;
        var h = game.world.height;

        this.bkg = game.add.tileSprite(0, 0, w, h, 'imgBkg');

        this.paddle = game.add.sprite(0, 0, 'imgPaddle');
        this.paddle.anchor.setTo(0.5, 1);
        this.paddleHalf = this.paddle.width / 2;

        this.sfxHitBrick = game.add.audio('sfxHitBrick');
        this.sfxHitPaddle = game.add.audio('sfxHitPaddle');
        this.bgmMusic = game.add.audio('bgmMusic');
        this.bgmMusic.loop = true;
        this.bgmMusic.play();

        this.sfxLoseLife =game.add.audio('sfxLoseLife');
        h = this.paddle.height;
        var blackLine = game.add.tileSprite(0, 0, w, h, 'imgBlack');
        blackLine.anchor.set(0, 1);
        blackLine.y = game.world.height;


        this.txt_lives = game.add.text(0, 0, g_txt_lives + this .lives);
        this.txt_lives.fontSize = 18;
        this.txt_lives.fill = '#ffffff';
        this.txt_lives.align = 'left';
        this.txt_lives.font = 'Overlock';
        this.txt_lives.anchor.set(0, 1);
        this.txt_lives.y = game.world.height;

        var txt_config = {
            font: '18px Overlock',
            fill :'#ffffff', 
            align:'right'
        };
        this.txt_points = game.add.text(0, 0, this.points + g_txt_points, txt_config);
        this.txt_points.anchor.set(1);
        this.txt_points.x = game.world.width;
        this.txt_points.y = game.world.height;

        game.physics.arcade.enable(this.paddle);
        this.paddle.body.enable = true;
        this.paddle.body.immovable = true;

        this.bricks = game.add.group();
        this.bricks.enableBody = true;
        this.bricks.bodyType = Phaser.Physics.ARCADE;

        this.paddleVelX = 500 / 1000;
        this.prevX = game.input.x;


        this.ball = game.add.sprite(0, 0, 'imgBall');
        game.physics.arcade.enable(this.ball);
        this.ball.body.enable = true;
        this.ball.body.bounce.set(1);
        this.ball.body.collideWorldBounds = true;
        this.ball.iniVelX = 200;
        this.ball.iniVelY = -300;
        this.ball.isShot = false;
        this.resetPaddle();

        this.num_cols = 10;
        this.num_rows = 4;
        var brick_images = [
            'imgBrickGreen',
            'imgBrickPurple',
            'imgBrickRed',
            'imgBrickYellow'
        ];
        var i,j;
        for (i = 0; i < this.num_rows; i++) {
            var img = brick_images[i];
            for (j = 0; j < this.num_cols; j++) {
                var brick = this.bricks.create(0, 0, img);
                brick.body.immovable =true;
                brick.x = brick.width * j;
                brick.y = brick.height * i;
            }
        }

        game.input.onDown.add(this.onDown, this);
        game.input.onDown.add(this.onUp, this);
    },
    update : function () {
        game.physics.arcade.collide(this.ball, this.paddle, this.hitPaddle, null, this);

        game.physics.arcade.collide(this.ball, this.bricks, this.removeBrick, null, this);

        var is_left_down = game.input.keyboard.isDown(Phaser.Keyboard.LEFT);    
        var is_right_down = game.input.keyboard.isDown(Phaser.Keyboard.RIGHT);    
        
        if (this.prevX != game.input.x) {
            this.paddle.x = game.input.x;
        } else if (is_right_down && !is_left_down) {
            // go right
            this.paddle.x += this.paddleVelX * game.time.physicsElapsedMS;
        } else if (is_left_down && !is_right_down) {
            this.paddle.x -= this.paddleVelX * game.time.physicsElapsedMS;
        }
        this.prevX = game.input.x;
        if (game.device.touch && this.touchActive) {
            this.touchOldX = this.touchNewX;
            this.touchNewX = game.input.x;
            this.touchMove = 0;

            if (this.touchOldX != undefined && this.touchNewX != undefined) {
                this.touchMove = this.touchNewX - this.touchOldX;
            }
            this.paddle.x+=this.touchMove;
        }

        // Guard against going too far left
        if (this.paddle.x - this.paddleHalf < 0) {
            this.paddle.x = 0 + this.paddleHalf;
        }
        if (this.paddle.x + this.paddleHalf > game.world.width) {
            this.paddle.x = game.world.width - this.paddleHalf;
        }
        if (this.ball.isShot == false) {
            this.ball.x = this.paddle.x;
        }

        if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.shootBall();
        }

        // it doesn't show quite say where this belongs
        this.ball.checkWorldBounds = true;
        this.ball.events.onOutOfBounds.add(this.loseLife, this);

        this.bkg.tilePosition.y+=1;
    },
    resetPaddle: function () {
        this.paddle.x = game.world.centerX;
        this.paddle.y = game.world.height - this.paddle.height;

        this.ball.x = this.paddle.x;
        this.ball.y = this.paddle.y - this.paddle.height;
        this.ball.isShot = false;
        this.ball.body.velocity.set(0);
    },
    shootBall : function () {
        if (this.ball.isShot) {
            return;
        }
        var vel_x = this.ball.iniVelX;
        var vel_y = this.ball.iniVelY;
        var rand = Math.floor(Math.random() * 2);;
        if (rand %2 ==0) {
            vel_x *=-1;
        }
        this.ball.isShot = true;
        var state = game.state.getCurrentState();
        this.ball.body.velocity.set(vel_x, vel_y);
        this.sfxHitPaddle.play();
    },
    removeBrick : function (ball, brick) {
        brick.kill();
        this.points+=10;
        this.txt_points.text = this.points + g_txt_points;
        this.sfxHitBrick.play();

        if (this.bricks.countLiving() == 0) {
            this.goToOver();
        }
    },
    hitPaddle: function (ball, paddle) {
        this.sfxHitPaddle.play();
    },
    loseLife: function () {
        this.sfxLoseLife.play();
        this.resetPaddle();
        this.lives--;

        this.txt_lives.text = g_txt_lives + this.lives;

        if (this.lives == 0) {
            this.goToOver();
        }
    },
    goToOver : function () {
        this.bgmMusic.stop();
        game.lives = this.lives;
        game.points = this.points;
        game.state.start('StateOver');
    },
    onDown : function () {
        this.shootBall();
        this.touchActive = true;
    },
    onUp : function () {
        this.touchOldX = undefined;
        this.touchNewX = undefined;
        this.touchActive = false;
    }

};
